﻿using Wallboard3.Jenkins.Models;

namespace Wallboard3.Jenkins.Interfaces
{
    public interface IJenkinsService
    {
        string GetBuildStatus(JenkinsSettings settings);
        string GetProjectNames(JenkinsSettings settings);

        string GetDetailsForProject(JenkinsSettings settings, string projectName);
    }
}