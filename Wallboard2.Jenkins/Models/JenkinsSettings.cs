﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wallboard3.Jenkins.Models
{
    public class JenkinsSettings
    {
        public User User { get; set; }
        public bool UseSampleFile { get; set; }
        public string BuildUrl { get; set; }
        public string ProjectDetailUrl { get; set; }
        public string JobsUrl { get; set; }
    }

    public class User
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ApiToken { get; set; }
    }
}
