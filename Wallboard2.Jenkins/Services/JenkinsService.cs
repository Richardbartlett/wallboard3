﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json.Linq;
using Wallboard3.Jenkins.Interfaces;
using Wallboard3.Jenkins.Models;

namespace Wallboard3.Jenkins.Services
{
    public class JenkinsService : IJenkinsService
    {

        HttpClient _client = new HttpClient();

        public string GetProjectNames(JenkinsSettings settings)
        {
            string output = string.Empty;

            if(settings.UseSampleFile)
            {
                var reader = new StreamReader(Path.Combine(System.AppContext.BaseDirectory, "SampleJson/SampleJobs.json"));
                output = reader.ReadToEnd();
            }
            else
            {
                try
                {
                    byte[] credBuffer = new UTF8Encoding().GetBytes(settings.User.UserName + ":" + settings.User.ApiToken);

                    _client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(credBuffer));
                    output = _client.GetStringAsync(settings.JobsUrl).Result;
                }
                catch (Exception ex)
                {
                }

            }

            return output;
        }


        public string GetBuildStatus(JenkinsSettings settings)
        {
            string output = string.Empty;
            if(settings.UseSampleFile)
            {
                var reader = new StreamReader(Path.Combine(System.AppContext.BaseDirectory, "SampleJson/SampleJson.json"));
                output = reader.ReadToEnd();

                //output = System.IO.File.ReadAllText(Path.Combine(System.AppContext.BaseDirectory, "SampleJson/SampleJson.json"));

            }
            else
            {
                // Do webservice call here
                byte[] credBuffer = new UTF8Encoding().GetBytes(settings.User.UserName + ":" + settings.User.ApiToken);

                _client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(credBuffer));
                output = _client.GetStringAsync(settings.BuildUrl).Result;
            }

            return output;
        }

        public string GetDetailsForProject(JenkinsSettings settings, string projectName)
        {
            string output = string.Empty;
            string projUrl = settings.ProjectDetailUrl.Replace("{projectName}", projectName);


            if(settings.UseSampleFile)
            {

                var reader = new StreamReader(Path.Combine(System.AppContext.BaseDirectory, "SampleJson/CrownDepth2.json"));
                output = reader.ReadToEnd();
                //output = System.IO.File.ReadAllText(Path.Combine(System.AppContext.BaseDirectory, "SampleJson/CrownDepth2.json"));                  
            }
            else
            {

                byte[] credBuffer = new UTF8Encoding().GetBytes(settings.User.UserName + ":" + settings.User.ApiToken);

                _client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(credBuffer));
                output = _client.GetStringAsync(projUrl).Result;
                
            }

            return output;
        }

    }
}
