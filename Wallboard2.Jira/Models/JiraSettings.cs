﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wallboard3.Jira.Models
{
    public class JiraSettings
    {
        public User User { get; set; }
        public string BuildUrl { get; set; }
        public bool UseSampleFile { get; set; }
    }

    public class User
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ApiToken { get; set; }
    }
}
