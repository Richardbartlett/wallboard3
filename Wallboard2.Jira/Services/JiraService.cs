﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Wallboard3.Jira.Interfaces;
using Wallboard3.Jira.Models;

namespace Wallboard3.Jira.Services
{
    public class JiraService : IJiraService
    {
        public string GetJira(JiraSettings settings, string clientName)
        {
            string response = string.Empty;

            if(settings.UseSampleFile)
            {
                var reader = new StreamReader(Path.Combine(System.AppContext.BaseDirectory, "SampleJson/jiraSampleJson.json"));
                response = reader.ReadToEnd();
            }
            else
            {

                var byteArray = Encoding.ASCII.GetBytes(string.Format("{0}:{1}", settings.User.UserName, settings.User.Password));
                //client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));

            }


            return response;
        }

    }
}
