﻿using System;
using System.Collections.Generic;
using System.Text;
using Octopus.Client.Model;
using Wallboard3.Octopus.Models;

namespace Wallboard3.Octopus.Interfaces
{
    public interface IOctopusService
    {
        DashboardResource GetDashboards(OctopusSettings settings);

        Object GetDashboardByClient(OctopusSettings settings, string clientName);
    }
}
