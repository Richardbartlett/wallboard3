﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Octopus.Client;
using Octopus.Client.Model;
using Wallboard3.Octopus.Interfaces;
using Wallboard3.Octopus.Models;

namespace Wallboard3.Octopus.Services
{
    public class OctopusService : IOctopusService
    {

        public DashboardResource GetDashboards(OctopusSettings settings)
        {
            DashboardResource dashboard;

            if (settings.UseSampleFile)
            {
                string s = System.IO.File.ReadAllText(Path.Combine(System.AppContext.BaseDirectory, "SampleJson/SampleJson.json"));
                dashboard = JsonConvert.DeserializeObject<DashboardResource>(s);
            }
            else
            {
                //var server = "http://octopus.mccannci.net:81";
                //var apiKey = "API-4PBTHSF4JXAA1FXV2NFRAPIYLAO";
                var endpoint = new OctopusServerEndpoint(settings.BuildUrl, settings.User.ApiToken);

                using (var client = OctopusAsyncClient.Create(endpoint).Result)
                {
                    dashboard = client.Repository.Dashboards.GetDashboard().Result;
                    //resource = client.Repository.Dashboards.GetDynamicDashboard(new string[] { "Projects-42" }, new string[] { "Environments-41", "Environments-42"}).Result;
                }
            }

            return dashboard;
        }


        public Object GetDashboardByClient(OctopusSettings settings, string clientName)
        {

            DashboardResource dashboard;

            if (settings.UseSampleFile)
            {
                var reader = new StreamReader(Path.Combine(System.AppContext.BaseDirectory, "SampleJson/OctopusSampleJson.json"));
                string s = reader.ReadToEnd();

                dashboard = JsonConvert.DeserializeObject<DashboardResource>(s);
                dashboard.Projects.RemoveAll(x => x.Name != clientName);
            }
            else
            {
                //var server = "http://octopus.mccannci.net:81";
                //var apiKey = "API-4PBTHSF4JXAA1FXV2NFRAPIYLAO";
                var endpoint = new OctopusServerEndpoint(settings.BuildUrl, settings.User.ApiToken);

                using (var client = OctopusAsyncClient.Create(endpoint).Result)
                {
                    dashboard = client.Repository.Dashboards.GetDashboard().Result;
                    //resource = client.Repository.Dashboards.GetDynamicDashboard(new string[] { "Projects-42" }, new string[] { "Environments-41", "Environments-42"}).Result;
                    dashboard.Projects.RemoveAll(x => x.Name != clientName);
                }
            }

            //return dashboard;

            dynamic response = new ExpandoObject();
            NameValueCollection nvc = new NameValueCollection();

            foreach(var proj in dashboard.Projects.Where(x => x.Name == clientName))
            {
                foreach(var env in dashboard.Environments)
                {
                    nvc.Add(env.Name, dashboard.Items.Where(x => x.EnvironmentId == env.Id && x.ProjectId == proj.Id).FirstOrDefault().ReleaseVersion);
                }
            }


            //var QAId = dashboard.Environments.Where(x => x.Name == "QA").FirstOrDefault().Id;
            //var DEVId = dashboard.Environments.Where(x => x.Name == "DEV").FirstOrDefault().Id;
            //var response = new
            //{
            //    QA = new
            //    {
            //        Builds = dashboard.Items.Where(x => x.EnvironmentId == QAId && x.IsCurrent).FirstOrDefault().ReleaseVersion
            //    },
            //    DEV = new
            //    {
            //        Builds = dashboard.Items.Where(x => x.EnvironmentId == DEVId && x.IsCurrent).FirstOrDefault().ReleaseVersion
            //    }
            //};

            return nvc.AllKeys.ToDictionary(x => x, y => nvc[y]);

        }

    }
}
