﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wallboard3.Pingdom.Enums
{
    public enum PingdomStatusEnum
    {
        up = 0,
        down,
        unconfirmed_down,
        paused,
        unknown
    }
}
