﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Wallboard3.Pingdom.Enums;
using Wallboard3.Pingdom.Models;

namespace Wallboard3.Pingdom.Interfaces
{
    public interface IPingdomService
    {
        Task<Dictionary<string, PingdomResponse>> GetPingdomChecks(List<PingdomAccount> accounts, bool simulate=false);

        PingdomStatusEnum SimulatePingdomDown();
    }
}
