﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wallboard3.Pingdom.Models
{
    public class PingdomSettings
    {
        public List<PingdomAccount> Settings { get; set; }
    }

    public class PingdomAccount
    {
        public string Account { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string PingdomCheckUrl { get; set; }
        public string AppKey { get; set; }
        public string Logo { get; set; }
    }
}
