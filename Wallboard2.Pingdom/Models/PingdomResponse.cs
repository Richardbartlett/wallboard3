﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Text;
using Wallboard3.Pingdom.Enums;

namespace Wallboard3.Pingdom.Models
{
    public class PingdomResponse
    {
        public List<PingdomResponseItem> checks { get; set; }
        public PingdomResponseCounters counts { get; set; }
    }

    public class PingdomResponseCounters
    {
        public int total { get; set; }
        public int limited { get; set; }
        public int filtered { get; set; }
    }

    public class PingdomResponseItem
    {
        public string ID { get; set; }
        public int Created { get; set; }
        public string Name { get; set; }
        public string Hostname { get; set; }
        public string Resolution { get; set; }
        public int LastErrorTime { get; set; }
        public int LastResponseTime { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public PingdomStatusEnum Status { get; set; }
        public string[] Tags { get; set; }

        public string Logo { get; set; }
    }
}
