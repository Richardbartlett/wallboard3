﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wallboard3.Pingdom.Models
{
    public class PingdomResponseError
    {
        public string statuscode { get; set; }
        public string statusdesc { get; set; }
        public string errormessage { get; set; }
    }
}
