﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Wallboard3.Pingdom.Enums;
using Wallboard3.Pingdom.Interfaces;
using Wallboard3.Pingdom.Models;

namespace Wallboard3.Pingdom.Services
{
    public class PingdomService : IPingdomService
    {
        private static readonly Random getRandom = new Random();

        public async Task<Dictionary<string, PingdomResponse>> GetPingdomChecks(List<PingdomAccount> accounts, bool simulate=false)
        {
            if (accounts == null)
                return null;

            Dictionary<string, PingdomResponse> output = new Dictionary<string, PingdomResponse>();

            int randomNumber = getRandom.Next(0, 100);


            foreach (PingdomAccount acc in accounts)
            {
                Stream s = await GetPingdomResponse(acc.PingdomCheckUrl, acc.Username, acc.Password, acc.AppKey);

                if(s != null)
                {
                    StreamReader reader = new StreamReader(s);

                    PingdomResponse r = JsonConvert.DeserializeObject<PingdomResponse>(reader.ReadToEnd());

                    if(simulate)
                    {
                        if(randomNumber <= 50)
                        {
                            foreach (PingdomResponseItem pc in r.checks)
                            {
                                pc.Status = SimulatePingdomDown();
                            }
                        }
                    }




                    //string str = reader.ReadToEnd();
                    //List<PingdomResponse> r1 = JsonConvert.DeserializeObject<List<PingdomResponse>>(reader.ReadToEnd());
                    output.Add(acc.Account, r);
                }
            }

            return output;
        }

        private async Task<Stream> GetPingdomResponse(string url, string userName, string password, string appKey)
        {
            Stream s = null;

            using (HttpClient client = new HttpClient())
            {
                var byteArray = Encoding.ASCII.GetBytes(string.Format("{0}:{1}", userName, password));
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                client.DefaultRequestHeaders.Add("App-Key", appKey);
                try
                {
                    s = await client.GetStreamAsync(url);
                }
                catch (Exception ex)
                {

                }
            }

            return s;
        }

        public PingdomStatusEnum SimulatePingdomDown()
        {
            PingdomStatusEnum status = PingdomStatusEnum.up;

            int randomNumber = getRandom.Next(1, 101);

            if (randomNumber >= 0 && randomNumber <= 3) { status = PingdomStatusEnum.down; }
            if (randomNumber >= 4 && randomNumber <= 6) { status = PingdomStatusEnum.unconfirmed_down; }
            if (randomNumber >= 7 && randomNumber <= 12) { status = PingdomStatusEnum.paused; }
            if (randomNumber >= 13 && randomNumber <= 20) { status = PingdomStatusEnum.unknown; }
            if (randomNumber >= 21 && randomNumber <= 101) { status = PingdomStatusEnum.up; }

            return status;
        }


    }
}
