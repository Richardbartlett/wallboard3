﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wallboard3.Sonarqube.Models;

namespace Wallboard3.Sonarqube.Interfaces
{
    public interface ISonarqubeService
    {
        string GetSonarqubeDetailResponse(SonarqubeSettings settings, string clientName);
    }
}
