﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wallboard3.Sonarqube.Models
{
    public class SonarqubeSettings
    {
        public User User { get; set; }
        public string BuildUrl { get; set; }        
        public bool UseSampleFile { get; set; }
    }

    public class User
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ApiToken { get; set; }
    }
}
