﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Wallboard3.Sonarqube.Interfaces;
using Wallboard3.Sonarqube.Models;

namespace Wallboard3.Sonarqube.Services
{
    public class SonarqubeService : ISonarqubeService
    {
        public string GetSonarqubeDetailResponse(SonarqubeSettings settings, string clientName)
        {
            string response = string.Empty;
            
            if(settings.UseSampleFile)
            {
                var reader = new StreamReader(Path.Combine(System.AppContext.BaseDirectory, "SampleJson/sonarqubeSampleJson.json"));
                response = reader.ReadToEnd();
            }
            else
            {
                Stream s = null;

                using (HttpClient client = new HttpClient())
                {
                    //var byteArray = Encoding.ASCII.GetBytes(string.Format("{0}:{1}", userName, password));
                    //client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", appKey);

                    try
                    {
                        string url = settings.BuildUrl.Replace("{repoSlug}", clientName);

                        s = client.GetStreamAsync(url).Result;
                        response = new StreamReader(s).ReadToEnd();
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }

            return response;
        }

    }
}
