﻿using System;
using System.Collections.Generic;
using System.Text;
using Wallboard3.Bitbucket.Models;

namespace Wallboard3.Bitbucket.Interfaces
{
    public interface IBitbucketService
    {
        string GetPullRequests(BitbucketSettings settings, string clientName);
        string GetPullRequestsByUsername(BitbucketSettings settings, string username);
    }
}
