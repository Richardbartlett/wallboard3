﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wallboard3.Bitbucket.Models
{
    public class BitbucketSettings
    {        
        public User User { get; set; }
        public string RepoSlug { get; set; }
        public bool UseSampleFile { get; set; }
        public string PullRequestSlug { get; set; }
    }

    public class User
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ApiToken { get; set; }
        public string PullRequestUserName { get; set; }
    }
}
