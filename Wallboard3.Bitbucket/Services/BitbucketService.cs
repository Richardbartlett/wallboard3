﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using Wallboard3.Bitbucket.Interfaces;
using Wallboard3.Bitbucket.Models;

namespace Wallboard3.Bitbucket.Services
{
    public class BitbucketService : IBitbucketService
    {

        public string GetPullRequestsByUsername(BitbucketSettings settings, string username)
        {
            string response = string.Empty;

            HttpClient _client = new HttpClient();
            string url = settings.RepoSlug.Replace("{PullRequestUserName}", username).Replace("{bitbucketUsername}", settings.User.PullRequestUserName);
            var byteArray = Encoding.ASCII.GetBytes(string.Format("{0}:{1}", settings.User.UserName, settings.User.Password));
            _client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));

            try
            {
                HttpResponseMessage r = _client.GetAsync(url).Result;
                response = r.Content.ReadAsStringAsync().Result;
            }
            catch (Exception ex)
            {
                response = string.Empty;
            }

            return response;
        }

        public string GetPullRequests(BitbucketSettings settings, string clientName)
        {
            string response = string.Empty;

            if (settings.UseSampleFile)
            {
                response = System.IO.File.ReadAllText(Path.Combine(System.AppContext.BaseDirectory, "SampleJson/BitbucketSampleJson.json"));
            }
            else
            {
                HttpClient _client = new HttpClient();
                string url = settings.RepoSlug.Replace("{repoSlug}", clientName).Replace("{bitbucketUsername}", settings.User.PullRequestUserName);
                var byteArray = Encoding.ASCII.GetBytes(string.Format("{0}:{1}", settings.User.UserName, settings.User.Password));
                _client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));

                try
                {
                    HttpResponseMessage r = _client.GetAsync(url).Result;
                    response = r.Content.ReadAsStringAsync().Result;
                }
                catch (Exception ex)
                {
                    response = string.Empty;
                }

            }

            return response;
        }
    }
}
