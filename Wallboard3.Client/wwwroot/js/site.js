toastr.options = {
    "closeButton": false,
    "debug": true,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-bottom-full-width",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
}



$(document).ready(function () {

    //var uri = "ws://" + "qdos.ddns.net:8010" + "/ws";
    //var uri = "ws://localhost:55627/ws";
    var uri = "ws://rbplayground.azurewebsites.net/ws";
    var socket = new FancyWebSocket(uri);

    var $pingdomModal = $("#pingdomModal");
    var popup = new Foundation.Reveal($pingdomModal);

    socket.bind('PullRequestCreated', function (data) {
        console.log("data = ", data);

        var frombranch = data.pullrequest.source.branch.name;
        var tobranch = data.pullrequest.destination.branch.name;

        console.log(frombranch);
        console.log(tobranch);

        toastr.success(data.pullrequest.summary.raw, data.pullrequest.author.display_name + " : " + frombranch + " > " + tobranch);
        console.log("a pull request has been created");
    });

    socket.bind("pingdom", function (data) {
        var $pingdomModalVisible = $pingdomModal.is(":visible");
        var currentState = data.current_state;

        console.log("current state: ", currentState);
        console.log("reveal visible = ", $pingdomModalVisible);

        switch (currentState) {
            case "UP":
                // check if check_id is in list, if it is, remove it.
                var found_items = $pingdomModal.children("#" + data.check_id);
                console.log("UP - FoundItems", found_items);
                console.log("UP - FoundItems length", found_items.length);
                $(found_items).addClass("up");
                var timeout = window.setTimeout(function () {
                    $(found_items).fadeOut("slow", "swing", function () {
                        found_items.remove();

                        console.log("pingdom item", $pingdomModal);
                        console.log($pingdomModal.find(".pingdom-item").length);
                        if ($pingdomModal.find(".pingdom-item").length == 0) {
                            console.log("1");
                            if ($pingdomModalVisible == true) {
                                console.log("2");
                                popup.close();
                            }
                        }

                    });

                }, 2000);

                break;

            case "DOWN":
                console.log("shdfjkdshfjksdfjksdhf");
                var found_items = $pingdomModal.children("#" + data.check_id);
                
                if (found_items.length == 0) {

                    var modalSource = document.getElementById("pingdomModalItemTemplate").innerHTML;
                    var modalTemplate = Handlebars.compile(modalSource);
                    var modalHtml = modalTemplate(data);
                    console.log("modal hhtml = ", modalHtml);
                    $pingdomModal.append(modalHtml);

                    if (!$pingdomModalVisible) {
                        popup.open();
                    }
                }                
                break;

            default:
                break;
        }        
        console.log("pingdom webhook", data);
    });

    socket.bind("tester", function (data) {
        console.log("this is the tester");
    });

    socket.bind("open", function (data) {
        console.log("opened..");
    });

    $(document).foundation();
})
