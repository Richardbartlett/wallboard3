﻿var app = app || {};

app.sockets = (function($) {
	"use strict";
	var socket,

	createSocket = function(socketUrl) {
		socket = new WebSocket(socketUrl);
		socket.onopen = function(event) {
			console.log("open connection");
		};
		socket.onmessage = function(event) {
		console.log("onmessage")
		};
		socket.onerror = function(event) {
			console.log("error " + event.data);
		};

		return socket;
	},


	init = function() {
		
	}


	return {
		init: init,
		createSocket: createSocket
	}
}(jQuery));