﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Wallboard3.Models.BitBucket
{
    public class CreatedPullRequest
    {
        public Pullrequest pullrequest { get; set; }
        public Actor actor { get; set; }
        public Repository2 repository { get; set; }
    }

    public class Pullrequest
    {
        public object merge_commit { get; set; }
        public string description { get; set; }
        public Links links { get; set; }
        public string title { get; set; }
        public bool close_source_branch { get; set; }
        public object[] reviewers { get; set; }
        public Destination destination { get; set; }
        public int comment_count { get; set; }
        public object closed_by { get; set; }
        public Summary summary { get; set; }
        public Source source { get; set; }
        public DateTime created_on { get; set; }
        public string state { get; set; }
        public int task_count { get; set; }
        public object[] participants { get; set; }
        public string reason { get; set; }
        public DateTime updated_on { get; set; }
        public Author author { get; set; }
        public string type { get; set; }
        public int id { get; set; }
    }

    public class Links
    {
        public Decline decline { get; set; }
        public Commits commits { get; set; }
        public Self self { get; set; }
        public Comments comments { get; set; }
        public Merge merge { get; set; }
        public Html html { get; set; }
        public Activity activity { get; set; }
        public Diff diff { get; set; }
        public Approve approve { get; set; }
        public Statuses statuses { get; set; }
    }

    public class Decline
    {
        public string href { get; set; }
    }

    public class Commits
    {
        public string href { get; set; }
    }

    public class Self
    {
        public string href { get; set; }
    }

    public class Comments
    {
        public string href { get; set; }
    }

    public class Merge
    {
        public string href { get; set; }
    }

    public class Html
    {
        public string href { get; set; }
    }

    public class Activity
    {
        public string href { get; set; }
    }

    public class Diff
    {
        public string href { get; set; }
    }

    public class Approve
    {
        public string href { get; set; }
    }

    public class Statuses
    {
        public string href { get; set; }
    }

    public class Destination
    {
        public Commit commit { get; set; }
        public Branch branch { get; set; }
        public Repository repository { get; set; }
    }

    public class Commit
    {
        public string type { get; set; }
        public string hash { get; set; }
        public Links1 links { get; set; }
    }

    public class Links1
    {
        public Self1 self { get; set; }
        public Html1 html { get; set; }
    }

    public class Self1
    {
        public string href { get; set; }
    }

    public class Html1
    {
        public string href { get; set; }
    }

    public class Branch
    {
        public string name { get; set; }
    }

    public class Repository
    {
        public string full_name { get; set; }
        public string type { get; set; }
        public string name { get; set; }
        public Links2 links { get; set; }
        public string uuid { get; set; }
    }

    public class Links2
    {
        public Self2 self { get; set; }
        public Html2 html { get; set; }
        public Avatar avatar { get; set; }
    }

    public class Self2
    {
        public string href { get; set; }
    }

    public class Html2
    {
        public string href { get; set; }
    }

    public class Avatar
    {
        public string href { get; set; }
    }

    public class Summary
    {
        public string raw { get; set; }
        public string markup { get; set; }
        public string html { get; set; }
        public string type { get; set; }
    }

    public class Source
    {
        public Commit1 commit { get; set; }
        public Branch1 branch { get; set; }
        public Repository1 repository { get; set; }
    }

    public class Commit1
    {
        public string type { get; set; }
        public string hash { get; set; }
        public Links3 links { get; set; }
    }

    public class Links3
    {
        public Self3 self { get; set; }
        public Html3 html { get; set; }
    }

    public class Self3
    {
        public string href { get; set; }
    }

    public class Html3
    {
        public string href { get; set; }
    }

    public class Branch1
    {
        public string name { get; set; }
    }

    public class Repository1
    {
        public string full_name { get; set; }
        public string type { get; set; }
        public string name { get; set; }
        public Links4 links { get; set; }
        public string uuid { get; set; }
    }

    public class Links4
    {
        public Self4 self { get; set; }
        public Html4 html { get; set; }
        public Avatar1 avatar { get; set; }
    }

    public class Self4
    {
        public string href { get; set; }
    }

    public class Html4
    {
        public string href { get; set; }
    }

    public class Avatar1
    {
        public string href { get; set; }
    }

    public class Author
    {
        public string username { get; set; }
        public string display_name { get; set; }
        public string account_id { get; set; }
        public Links5 links { get; set; }
        public string type { get; set; }
        public string uuid { get; set; }
    }

    public class Links5
    {
        public Self5 self { get; set; }
        public Html5 html { get; set; }
        public Avatar2 avatar { get; set; }
    }

    public class Self5
    {
        public string href { get; set; }
    }

    public class Html5
    {
        public string href { get; set; }
    }

    public class Avatar2
    {
        public string href { get; set; }
    }

    public class Actor
    {
        public string username { get; set; }
        public string display_name { get; set; }
        public string account_id { get; set; }
        public Links6 links { get; set; }
        public string type { get; set; }
        public string uuid { get; set; }
    }

    public class Links6
    {
        public Self6 self { get; set; }
        public Html6 html { get; set; }
        public Avatar3 avatar { get; set; }
    }

    public class Self6
    {
        public string href { get; set; }
    }

    public class Html6
    {
        public string href { get; set; }
    }

    public class Avatar3
    {
        public string href { get; set; }
    }

    public class Repository2
    {
        public string scm { get; set; }
        public string website { get; set; }
        public string name { get; set; }
        public Links7 links { get; set; }
        public string full_name { get; set; }
        public Owner owner { get; set; }
        public string type { get; set; }
        public bool is_private { get; set; }
        public string uuid { get; set; }
    }

    public class Links7
    {
        public Self7 self { get; set; }
        public Html7 html { get; set; }
        public Avatar4 avatar { get; set; }
    }

    public class Self7
    {
        public string href { get; set; }
    }

    public class Html7
    {
        public string href { get; set; }
    }

    public class Avatar4
    {
        public string href { get; set; }
    }

    public class Owner
    {
        public string username { get; set; }
        public string display_name { get; set; }
        public string account_id { get; set; }
        public Links8 links { get; set; }
        public string type { get; set; }
        public string uuid { get; set; }
    }

    public class Links8
    {
        public Self8 self { get; set; }
        public Html8 html { get; set; }
        public Avatar5 avatar { get; set; }
    }

    public class Self8
    {
        public string href { get; set; }
    }

    public class Html8
    {
        public string href { get; set; }
    }

    public class Avatar5
    {
        public string href { get; set; }
    }

}
