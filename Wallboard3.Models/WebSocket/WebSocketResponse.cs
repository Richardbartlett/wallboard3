﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Wallboard3.Models.WebSocket
{
    public class WebSocketResponse
    {
        [JsonProperty("event")]
        public string Event { get; set; }

        [JsonProperty("payload")]
        public Object Payload { get; set; }
    }
}
