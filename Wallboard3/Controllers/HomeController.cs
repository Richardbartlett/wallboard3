﻿using Microsoft.AspNetCore.Mvc;
using Wallboard3.Models.WebSocket;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.Annotations;
using Wallboard3.Bitbucket.Services;
using Wallboard3.Bitbucket.Interfaces;
using Wallboard3.Bitbucket.Models;
using Microsoft.Extensions.Options;
using Wallboard3.Extensions;

namespace Wallboard3.Controllers
{
    [Produces("application/json")]
    public class HomeController : Controller
    {

        private BitbucketMessageHandler serv;
        private IBitbucketService _bitbucketService;
        private BitbucketSettings _bitbucketSettings;

        public HomeController(BitbucketMessageHandler bitbucket1, 
                                IBitbucketService bitbucketService,
                                IOptions<BitbucketSettings> bitbucketSettings)
        {
            serv = bitbucket1;
            _bitbucketService = bitbucketService;
            _bitbucketSettings = bitbucketSettings.Value;
        }

        public ActionResult Test()
        {
            return Content("sfhjksdfh");
        }

        /// <summary>
        /// Created Pull Request
        /// </summary>
        /// <param name="pullRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerOperation("Created Pull Request")]
        [Route("CreatePullRequest")]
        public IActionResult CreatePullRequest([FromBody]object pullRequest)
        {
            CreateWebSocketResponse("PullRequestCreated", pullRequest);

            return Ok();
        }

        /// <summary>
        /// Updated Pull Request
        /// </summary>
        /// <param name="pullRequest"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult UpdatedPullRequest([FromBody]object pullRequest)
        {
            CreateWebSocketResponse("PullRequestUpdated", pullRequest);

            return Ok(pullRequest);
        }

        /// <summary>
        /// Generic WebSocket Sender
        /// </summary>
        /// <param name="payload"></param>
        /// <param name="eventName"></param>
        /// <returns></returns>
        [Route("SendWebSocketRequest")]
        [HttpPost]
        public IActionResult SendWebSocketRequest(string eventName, [FromBody]object payload)
        {
            CreateWebSocketResponse(eventName, payload);

            return Ok();
        }

        private void CreateWebSocketResponse(string action, object pullRequest)
        {
            WebSocketResponse wsr = new WebSocketResponse();

            wsr.Event = action;
            wsr.Payload = pullRequest;

            string retJson = JsonConvert.SerializeObject(wsr);
            serv.SendMessageToAllAsync(retJson);
        }



        /// <summary>
        /// Gets pull requests for user defined in appSettings.json
        /// </summary>
        /// <returns></returns>
        [Route("GetPullRequests")]
        [HttpGet] 
        public IActionResult GetPullRequests()
        {
           string result = _bitbucketService.GetPullRequestsByUsername(_bitbucketSettings, _bitbucketSettings.User.PullRequestUserName);

            return Ok(result.TryParse());
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        //public IActionResult Error()
        //{
        //    return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        //}
    }
}
