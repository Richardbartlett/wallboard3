﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.WebSockets;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Wallboard3.Bitbucket.Interfaces;
using Wallboard3.Bitbucket.Models;
using Wallboard3.Bitbucket.Services;

namespace Wallboard3
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<BitbucketSettings>(Configuration.GetSection("BitbucketSettings"));

            services.AddMvc();            
            services.AddWebSocketManager();

            services.AddScoped<IBitbucketService, BitbucketService>();


            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc(
                    "v1",
                    new Swashbuckle.AspNetCore.Swagger.Info
                    {
                        Title = "Wallboard 3",
                        Description = "Wallboard 3",
                        Version = "v1"
                    });


                //var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var basePath = System.AppContext.BaseDirectory;
                var xmlPath = Path.Combine(basePath, "Wallboard3.xml");
                options.IncludeXmlComments(xmlPath);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IServiceProvider serviceProvider)
        {
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Wallboard 3");
            });

            app.UseWebSockets();

            app.MapWebSocketManager("/ws", serviceProvider.GetService<BitbucketMessageHandler>());


        }
    }
}
